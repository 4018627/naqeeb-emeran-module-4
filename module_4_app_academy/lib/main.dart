import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Naqeeb Emeran - MTN App Academy Module 4 Assignment',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.amber)    // Constant theme for app
            .copyWith(secondary: Colors.black),
      ),
      home: const MyHomePage(
          title: 'Naqeeb Emeran - MTN App Academy Module 4 Assignment'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: SafeArea(
            child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height,
                padding:
                    const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Text(
                            "Hello There!",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 40),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          Text(
                            "Welcome to my MTN App Academy Module 4 Assignment!",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.grey[700], fontSize: 15),
                          ),
                          Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.all(30),
                            child: SizedBox(
                              height: 155.0,
                              child: Image.asset(
                                "assets/mtnbuslogo.jpg",
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return const Login();
                              }));
                            },
                            child: const Text(
                              "Login",
                              style: TextStyle(fontSize: 18),
                            ),
                          ),
                          const SizedBox(
                            height: 20.0,
                          ),
                          const Text("Don't have an account?"),
                          TextButton(
                              onPressed: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return const Signup();
                                }));
                              },
                              child: const Text("Sign up"))
                        ],
                      ),
                    ]))));
  }
}

class Signup extends StatelessWidget {
  const Signup({Key? key}) : super(key: key);
  final String title = "Sign Up";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 40.0, bottom: 30.0),
                child: Image.asset(
                  "assets/mtnbuslogo.jpg",
                  fit: BoxFit.scaleDown,
                ),
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Full Name",
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Email",
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Password",
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.fromLTRB(200.0, 10.0, 200.0, 10.0),
                  child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return const Dashboard();
                        }));
                      },
                      child: const Text(
                        "Sign up",
                        style: TextStyle(fontSize: 18),
                      )))
            ],
          ),
        ));
  }
}

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);
  final String title = "Login";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  top: 40.0, bottom: 30.0, left: 150, right: 150),
              child: Image.asset(
                "assets/mtnbuslogo.jpg",
                fit: BoxFit.contain,
              ),
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Email",
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Password",
                ),
              ),
            ),
            Padding(
                padding: const EdgeInsets.fromLTRB(200.0, 10.0, 200.0, 10.0),
                child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const Dashboard();
                      }));
                    },
                    child: const Text(
                      "Login",
                      style: TextStyle(fontSize: 18),
                    )))
          ],
        ),
      ),
    );
  }
}

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);
  final String title = "Dashboard";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
          child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).colorScheme.primary,
                    child: const Text("View Livestreams"),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const Vlives();
                      }));
                    }),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).colorScheme.primary,
                    child: const Text("View Gallery"),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const Gallery();
                      }));
                    }),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).colorScheme.primary,
                    child: const Text("FAQs"),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const FAQs();
                      }));
                    }),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).colorScheme.primary,
                    child: const Text("User Profile Edit"),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const EditProf();
                      }));
                    }),
              )
            ],
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: MaterialButton(
                  height: 100.0,
                  minWidth: 150.0,
                  color: Theme.of(context).colorScheme.primary,
                  child: const Text("Logout"),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return const MyApp();
                    }));
                  }),
            ),
          ])
        ],
      )),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.computer_rounded),
        backgroundColor: Theme.of(context).colorScheme.primary,
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return const Dashboard();
          }));
        },
      ),
    );
  }
}

class Vlives extends StatelessWidget {
  const Vlives({Key? key}) : super(key: key);
  final String title = "View Livestreams";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: SingleChildScrollView(
            child: Column(children: const <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 15.0),
            child: Text(
              "App Academy",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 5.0),
            child: Text(
              "Week 5",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 10.0),
            child: Text(
              "View",
              style:
                  TextStyle(fontSize: 20, decoration: TextDecoration.underline),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 30.0),
            child: Text(
              "May 24, 2022 (10:00 - 12:30)",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 5.0),
            child: Text(
              "Week 4",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 10.0),
            child: Text(
              "View",
              style:
                  TextStyle(fontSize: 20, decoration: TextDecoration.underline),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 30.0),
            child: Text(
              "May 17, 2022 (10:00 - 12:30)",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 5.0),
            child: Text(
              "Week 3",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 10.0),
            child: Text(
              "View",
              style:
                  TextStyle(fontSize: 20, decoration: TextDecoration.underline),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 30.0),
            child: Text(
              "May 10, 2022 (10:00 - 12:30)",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 5.0),
            child: Text(
              "Week 2",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 10.0),
            child: Text(
              "View",
              style:
                  TextStyle(fontSize: 20, decoration: TextDecoration.underline),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 30.0),
            child: Text(
              "May 3, 2022 (10:00 - 12:30)",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 5.0),
            child: Text(
              "Week 1",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 10.0),
            child: Text(
              "View",
              style:
                  TextStyle(fontSize: 20, decoration: TextDecoration.underline),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 30.0),
            child: Text(
              "April 26, 2022 (10:00 - 12:30)",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
        ])),
        floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
        floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.computer_rounded),
            backgroundColor: Theme.of(context).colorScheme.primary,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const Dashboard();
              }));
            }));
  }
}

class Gallery extends StatelessWidget {
  const Gallery({Key? key}) : super(key: key);
  final String title = "Gallery";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: ListView(children: [
          Column(
            children: [
              Column(children: [
                SizedBox(
                  child: Image.asset(
                    "assets/Gallery.png",
                    fit: BoxFit.scaleDown,
                  ),
                )
              ])
            ],
          ),
        ]),
        floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
        floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.computer_rounded),
            backgroundColor: Theme.of(context).colorScheme.primary,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const Dashboard();
              }));
            }));
  }
}

class FAQs extends StatelessWidget {
  const FAQs({Key? key}) : super(key: key);
  final String title = "FAQs";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: ListView(children: [
          Column(
            children: [
              Column(children: [
                SizedBox(
                  child: Image.asset(
                    "assets/FAQs.png",
                    fit: BoxFit.scaleDown,
                  ),
                )
              ])
            ],
          ),
        ]),
        floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
        floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.computer_rounded),
            backgroundColor: Theme.of(context).colorScheme.primary,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const Dashboard();
              }));
            }));
  }
}

class EditProf extends StatelessWidget {
  const EditProf({Key? key}) : super(key: key);
  final String title = "Edit Profile";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 40.0, bottom: 30.0),
                child: Image.asset(
                  "assets/user-profile.jpg",
                  height: 215,
                  width: 215,
                  
                ),
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Full Name",
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Email",
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Password",
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Date of Birth (dd/mm/yy)",
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.fromLTRB(200.0, 10.0, 200.0, 10.0),
                  child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return const Dashboard();
                        }));
                      },
                      child: const Text(
                        "Save Details",
                        style: TextStyle(fontSize: 18),

                      )))
            ],
          ),
        ));
  }
}
